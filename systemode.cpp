/*
x' = y;
y' = -x;

x(0) = 0, y(0) = 1;

x = sin(t), y = cos(t), x^2 + y^2 = 1
*/


// g++ systemode.cpp -o systemode && ./systemode
#include <iostream>

double f(double y){
    return y;
}

double g(double x){
    return -x;
}

int main(){
    double T, x1, y1;
    double x0 = 0.;
    double y0 = 1.;
    double t = 0.;
    
    const double h = 0.1;

    std::cout << "T = ";
    std::cin >> T;

    do {
        x1 = x0 + h*f(y0);
        y1 = y0 + h*g(x0);
        t += h;        
        std::cout << "x^2 + y^2 = " << x1*x1 + y1*y1 << std::endl;
        x0 = x1;
        y0 = y1;
    } while(t < T);
    
    return 0;
}
