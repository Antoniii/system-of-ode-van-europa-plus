![](https://gitlab.com/Antoniii/system-of-ode-van-europa-plus/-/raw/main/photo_2023-06-20_08-12-33.jpg)


## Sources

* [Методы Рунге-Кутта](https://stepanzh.github.io/computational_thermodynamics/ode/rungekutta.html)
* [Runge-Kutta 2nd order method to solve Differential equations](https://www.geeksforgeeks.org/runge-kutta-2nd-order-method-to-solve-differential-equations/)
